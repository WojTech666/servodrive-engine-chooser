package pl.edu.pw.wip.servodriveenginechooser.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.edu.pw.wip.servodriveenginechooser.entities.Engine;

import java.util.List;

public interface EngineRepository extends CrudRepository<Engine, Long> {

    @Query("SELECT e FROM Engine e where e.nominalPower >= :nominalPower and e.nominalMoment >= :nominalMoment and e.nominalSpeed >= :nominalSpeed and e.weight <= :weight and e.length <= :length")
    List<Engine> selectEngineBasedOnParameters(
            @Param("nominalPower") float nominalPower,
            @Param("nominalMoment") float nominalMoment,
            @Param("nominalSpeed") float nominalSpeed,
            @Param("weight") float weight,
            @Param("length") float length);

}
