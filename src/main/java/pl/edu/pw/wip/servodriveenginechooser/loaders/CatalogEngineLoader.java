package pl.edu.pw.wip.servodriveenginechooser.loaders;

import org.springframework.beans.factory.annotation.Value;
import pl.edu.pw.wip.servodriveenginechooser.models.NewEngine;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CatalogEngineLoader implements Loader{

    @Value("${catalog.servo.engine}")
    private String url;

    private List<NewEngine> engines;

    public CatalogEngineLoader() {
        this.engines = new ArrayList<>();
    }

    @Override
    public List<NewEngine> load() {
        String page = getPageFromURL();
        String servoTable = getServoTable(page);

        return generatesServoEngine(servoTable.replaceAll("rpm", "obr./min"));
    }

    private String getPageFromURL() {
        try {
            URL wobit = new URL(url);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(wobit.openStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String inputLine = "";
            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            bufferedReader.close();
            return stringBuilder.toString();
        } catch (IOException e) {
            throw new IllegalStateException("Error occurs during servo engines data loading.");
        }
    }

    private String getServoTable(String page) {
        return page.substring(page.indexOf("<table"), page.indexOf("</table>"));
    }

    private List<NewEngine> generatesServoEngine(String table) {
        String product = "<tr data-product=\"";
        String[] products = table.split(product);
        List<NewEngine> engines = new ArrayList<>();

        for (int i=1; i<products.length; i++) {
            engines.add(NewEngine.builder()
                    .name(getName(products[i]))
                    .nominalMoment(getMoment(products[i]))
                    .nominalPower(getPower(products[i]))
                    .nominalSpeed(getSpeed(products[i]))
                    .build());
        }
        return engines;
    }

    private String getName(String product) {
        return product.split("><")[0].replaceAll("\"", "");
    }

    private float getPower(String product) {
        return Float.valueOf(product.substring(product.indexOf("center\">"), product.indexOf(" W")).split(">")[1]);
    }

    private float getMoment(String product) {
        String[] elements = product.substring(product.indexOf("center\">"), product.indexOf(" Nm")).split(">");
        return Float.valueOf(elements[elements.length - 1]);
    }

    private float getSpeed(String product) {
        String[] elements = product.substring(product.indexOf("center\">"), product.indexOf(" obr./min")).split(">");
        return Float.valueOf(elements[elements.length - 1]);
    }
}
