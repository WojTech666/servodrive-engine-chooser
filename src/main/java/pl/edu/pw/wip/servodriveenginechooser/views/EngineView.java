package pl.edu.pw.wip.servodriveenginechooser.views;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pw.wip.servodriveenginechooser.entities.Engine;
import pl.edu.pw.wip.servodriveenginechooser.models.InputEngineData;
import pl.edu.pw.wip.servodriveenginechooser.models.NewEngine;
import pl.edu.pw.wip.servodriveenginechooser.service.EngineService;
import java.util.Date;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/engines")
public class EngineView {
    private EngineService engineService;

    @RequestMapping("/index")
    public String index(Model model){
        model.addAttribute("datetime", new Date());
        InputEngineData inputEngineData = new InputEngineData();
        model.addAttribute(inputEngineData);
        return "index";
    }

    @GetMapping("/all")
    public String getAllEngine(Model model) {
        model.addAttribute("engines", engineService.getAllEngines());
        return "engines";
    }

    @GetMapping("/add")
    public String addEngine() {
        return "new";
    }

    @PostMapping("/select")
    public String selectEngine(@ModelAttribute("inputEngineData") InputEngineData inputEngineData,
                               BindingResult result, Model model) {
        Engine optimalEngine;
        List<Engine> selectedEngines = engineService.selectEngine(inputEngineData);
        if(selectedEngines.size() != 0) {
            optimalEngine = engineService.selectTheMostOptimal(inputEngineData, selectedEngines);
        } else {
            optimalEngine = engineService.selectTheBestMatch(inputEngineData);
        }
        model.addAttribute("optimalEngine", optimalEngine);
        model.addAttribute("selectedEngines", selectedEngines);

        return "result";
    }

    @PostMapping("save")
    public String saveEngine(NewEngine newEngine) {
        engineService.addEngine(newEngine);

        return "index";
    }

}
