package pl.edu.pw.wip.servodriveenginechooser.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pw.wip.servodriveenginechooser.loaders.CatalogEngineLoader;
import pl.edu.pw.wip.servodriveenginechooser.models.NewEngine;
import java.util.List;

@RestController
@AllArgsConstructor
public class AvailableServoEngineController {
    private CatalogEngineLoader catalogEngineLoader;

    @GetMapping("/engines")
    public List<NewEngine> getEngines() {
        return catalogEngineLoader.load();
    }

}
