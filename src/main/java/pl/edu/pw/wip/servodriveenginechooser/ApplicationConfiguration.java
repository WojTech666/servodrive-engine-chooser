package pl.edu.pw.wip.servodriveenginechooser;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.pw.wip.servodriveenginechooser.loaders.CatalogEngineLoader;
import pl.edu.pw.wip.servodriveenginechooser.repository.EngineRepository;
import pl.edu.pw.wip.servodriveenginechooser.service.EngineService;
import pl.edu.pw.wip.servodriveenginechooser.views.EngineView;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public CatalogEngineLoader catalogEngineLoader() {
        return new CatalogEngineLoader();
    }

    @Bean
    public EngineView engineView(EngineService engineService) {
        return new EngineView(engineService);
    }

    @Bean
    public EngineService engineService(EngineRepository engineRepository) {
        return new EngineService(engineRepository);
    }

}
