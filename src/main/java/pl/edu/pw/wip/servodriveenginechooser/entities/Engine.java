package pl.edu.pw.wip.servodriveenginechooser.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Table(name = "Engines")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Engine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private long id;

    @Column(name="NAME")
    private String name;

    @Column(name = "NOMINAL_POWER")
    private float nominalPower;

    @Column(name = "NOMINAL_MOMENT")
    private float nominalMoment;

    @Column(name = "NOMINAL_SPEED")
    private float nominalSpeed;

    @Column(name = "WEIGHT")
    private float weight;

    @Column(name = "LENGTH")
    private float length;

    @Column(name = "COMMENT")
    private String comment;
}
