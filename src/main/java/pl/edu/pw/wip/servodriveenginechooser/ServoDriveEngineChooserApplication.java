package pl.edu.pw.wip.servodriveenginechooser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServoDriveEngineChooserApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServoDriveEngineChooserApplication.class, args);
	}

}
