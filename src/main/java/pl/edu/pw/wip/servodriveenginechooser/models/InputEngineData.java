package pl.edu.pw.wip.servodriveenginechooser.models;

import lombok.*;

/**
 * Model danych wejściowych uzupełnianych przez użytkownika, na ich podstawie bedą wykonywane dalsze obliczenia.
 *
 * float nominalPower - moc nominalna [W]
 * float nominalMoment - moment nominalny [Nm]
 * float nominalSpeed - prędkość nominalna [rpm]
 * float weight - masa mechanizmu [kg]
 * float length - długość mechanizmu [mm]
 */

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class InputEngineData {
    private float nominalPower;
    private float nominalMoment;
    private float nominalSpeed;
    private float weight;
    private float length;
}
