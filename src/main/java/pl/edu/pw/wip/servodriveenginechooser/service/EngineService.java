package pl.edu.pw.wip.servodriveenginechooser.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.pw.wip.servodriveenginechooser.entities.Engine;
import pl.edu.pw.wip.servodriveenginechooser.models.InputEngineData;
import pl.edu.pw.wip.servodriveenginechooser.models.NewEngine;
import pl.edu.pw.wip.servodriveenginechooser.repository.EngineRepository;
import java.util.*;

@Service
@AllArgsConstructor
public class EngineService {
    private EngineRepository engineRepository;

    public Iterable<Engine> getAllEngines() {
        return engineRepository.findAll();
    }

    public void addEngine(NewEngine newEngine) {
        Engine engine = Engine.builder()
                .name(newEngine.getName())
                .nominalPower(newEngine.getNominalMoment())
                .nominalSpeed(newEngine.getNominalSpeed())
                .nominalMoment(newEngine.getNominalMoment())
                .weight(newEngine.getWeight())
                .length(newEngine.getLength())
                .comment(newEngine.getComment())
                .build();

        engineRepository.save(engine);
    }

    public List<Engine> selectEngine(InputEngineData selected) {
        return engineRepository.selectEngineBasedOnParameters(
                selected.getNominalPower(),
                selected.getNominalMoment(),
                selected.getNominalSpeed(),
                selected.getWeight(),
                selected.getLength());
    }

    public Engine selectTheMostOptimal(InputEngineData data, List<Engine> engines) {
        HashMap<Float, Engine> optimals = new HashMap<>();
        ArrayList<Float> sums = new ArrayList<>();
        for(Engine eng : engines) {
            float sum = countDiffrenceParamsForEngine(eng, data);

            sums.add(sum);
            optimals.put(sum, eng);
        }

        Collections.sort(sums);
        return optimals.get(sums.get(0));
    }

    public Engine selectTheBestMatch(InputEngineData data) {
        List<Engine> all = new ArrayList<>();
        Iterable<Engine> allEngines = getAllEngines();
        allEngines.forEach(all::add);

        return selectTheMostOptimal(data, all);
    }

    private float countDiffrenceParamsForEngine(Engine engine, InputEngineData input) {
        float sum = Math.abs(input.getNominalMoment() - engine.getNominalMoment())
            + Math.abs(input.getNominalPower() - engine.getNominalPower())
            + Math.abs(input.getNominalSpeed() - engine.getNominalSpeed())
            + Math.abs(input.getWeight() - engine.getWeight())
            + Math.abs(input.getLength() - engine.getLength());
        return sum;
    }
}
