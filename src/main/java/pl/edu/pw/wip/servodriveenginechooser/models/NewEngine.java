package pl.edu.pw.wip.servodriveenginechooser.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@Getter
@ToString
public class NewEngine {
    private String name;
    private float nominalPower;
    private float nominalMoment;
    private float nominalSpeed;
    private float weight;
    private float length;
    private String comment;
}
