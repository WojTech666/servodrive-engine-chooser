package pl.edu.pw.wip.servodriveenginechooser.models;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Setter
@Getter
public class CatalogEngine {

    private List<NewEngine> engines;

}
