package pl.edu.pw.wip.servodriveenginechooser.loaders;

import pl.edu.pw.wip.servodriveenginechooser.models.NewEngine;
import java.util.List;

public interface Loader {

    public List<NewEngine> load();
}
